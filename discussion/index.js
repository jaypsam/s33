//console.log("Hello World");

//Javascript Synchorous and Asyncronous
//JS is by default syncronous, it means that only one statement is executed at a time

// console.log('Hello World');
// consol.log('Hello World Again')
// console.log("Bye-Bye")

// console.log("Hello World")
// for(i = 0; i<=1000; i++){
// 	console.log(i)
// };
// console.log("Hello Again")

// Asynchronous means that we can proceed to execute other statements/ code block, while time consuming code is running in the background.

// Getting all Post
// The fetch API allows you to asynchronously request for a resource(data).
// A "promise " is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value.

/*
	Syntax:
		fetch("URL");
*/

// A promise may be in one of the 3 possible states: fullfilled, rejected, or pending
console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

/*
	Syntax:
		fetch(URL);
		.then((response)=>{//linecode})
*/

fetch("https://jsonplaceholder.typicode.com/posts")
// By using the response.status we only check if the promise is fullfilled or rejected
// ".then" method captures the "response" object and returns another promise which eventually be "resolved" or "rejected"
// .then((response) => console.log(response.status))

// Use the "json" method from the "response" object to convert the data retrived into JSON format to be used in our application

//To pass the data
// .then((response) => response.json())
// // // Using multiple "then" methods creates a "promise chain".
// // .then((json) => console.log(json));

// // To access the title/ display each title of the post
// .then((json) =>{
// //print individually
// json.forEach(post => console.log(post.title))
// })

//+++++++++++++++++++++++++++++++++++++++++++++++++++++

async function fetchData(){

	// Await - waits for the "fetch" method to complete, then it stores the value in the result variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")
	//Result returned by fetch is a promise returned.
	console.log(result)
	// The returened "Response" is an object
	console.log(typeof result)
	// We cannot access the content of the "Response" by directly accessing it's body property
	console.log(result.body)

	// Converts the data from the "Response" object to JSON format.
	let json = await result.json()
	console.log(json)
}
//Need to invoke
fetchData();

//+++++++++++++++++++++++++++++++++++++++++++++++++++++
// Getting a specific post
// (retrieve, /posts/:id, GET)/ document

// ':id' is a wildcard where you can put the unique identifier as value to show specific data/ resources.
fetch("https://jsonplaceholder.typicode.com/posts/10")
.then((response) => response.json())
.then((json) => console.log(json))

// Creating a post
/*
	Stntax:
		fetch("URL", options)
		.then((response) => {line})
		.then((json) => {line of code})
*/

// Example input for adding/ post
fetch("https://jsonplaceholder.typicode.com/posts", {
	//HTTP Method
	method: "POST",
	// Specifies the content that it will pass in JSON format
	headers:{
		"Content-Type": "application/json"
	},
	// Sets the content body data of the "Request" object to be sent to the backend/ server
	body: JSON.stringify({
		title: "New Post",
		body: "Hello Batch 197",
		userId: 1
	})


})
// response of the server based on the request.
.then((response) => response.json())
// Parameter can be change (json)
.then((json) => console.log(json))

// Updating a post
// (update, /posts/:id, PUT)

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++
// PUT method is used to update the whole
fetch("https://jsonplaceholder.typicode.com/posts/10",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated post",
		body: "Hello again Batch 197!",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Patch
fetch("https://jsonplaceholder.typicode.com/posts/10",{
	method: "PATCH",
	headers:{
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// Deleting a post
// (delete, /post/:id, DELETE)
fetch("https://jsonplaceholder.typicode.com/posts/10", {
	method: "DELETE"
})










