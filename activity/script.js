console.log('Activity');
// Number 3

async function getTodos(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos');
    const responseJson = await response.json();
    console.log(responseJson);
    // 4
    const allTitles = responseJson.map((json)=> 'title: '+ json.title);
    console.log(allTitles);
    
}
getTodos();

// Number 5
fetch('https://jsonplaceholder.typicode.com/todos/45')
// .then((res)=>response.json())
.then((json)=>console.log(json));

//Number 6
fetch('https://jsonplaceholder.typicode.com/todos/45')
// .then((res)=>response.json())
.then((json)=>{
    console.log(`title: ${json.title} | completed: ${json.completed}`);
});

//Number 7
async function createTodo(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos',
    {
        method:"POST",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            userId: 6,
            completed: true,
            title: "New Post"
        })
    });
    
    const responseJson = await response.json();
    console.log(responseJson);
}
createTodo();

// Number 8
async function updateTodo(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/45',
    {
        method:"PUT",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            title: "this is an updated to do"
        })
    });
    
    const responseJson = await response.json();
    console.log(responseJson);
}
updateTodo();

//Number 9
async function updateTodoStructure(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/45',
    {
        method:"PUT",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            title: "this is a restructured to do",
            description: "new description",
            status: "completed",
            date_completed: '2022-08-26',
            userId: 999999
        })
    });
    
    const responseJson = await response.json();
    console.log(responseJson);
}
updateTodoStructure()

//Number 10
async function patchTodo(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/45',
    {
        method:"PATCH",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            title: "this is a patched to do",
        })
    });
    
    const responseJson = await response.json();
    console.log(responseJson);
}
patchTodo();

//Number 11
async function completeTodo(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/45',
    {
        method:"PATCH",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            completed: true,
            date_completed: '2022-08-26'
        })
    });
    
    const responseJson = await response.json();
    console.log(responseJson);
}
completeTodo();

//Number 12
async function deleteTodo(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/45',
    {
        method:"DELETE"
    });
    
    const responseJson = await response.json();
    console.log(responseJson);
    console.log('method deleted');
}
deleteTodo();
